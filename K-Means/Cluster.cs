﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K_Means
{
    class Cluster
    {
        List<double> coordinate = new List<double>();
        string nama;

        public Cluster(string nama, double[] coor)
        {
            this.nama = nama;
            for(int i = 0;i < coor.Length; i++)
            {
                this.coordinate.Add(coor[i]);
            }
        }

        public string getNama()
        {
            return this.nama;
        }

        public double getCoordinate(int idx)
        {
            return this.coordinate[idx];
        }

        public List<double> getCoordinate()
        {
            return this.coordinate;
        }

        public void setCoordinate(double val, int idx)
        {
            this.coordinate[idx] = val;
        }
    }
}
